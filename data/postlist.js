const post = (
    [
        {
            "id": 1,
            "title": "Orario provvisorio A.S. 2019-20",
            "thumbnail": "/static/img/orario.jpg",
            "description": "Si comunica che sul sito sarà pubblicato in data odierna l’orario provvisorio in vigore da giovedì 12 settembre p.v.",
            "links": [
                {
                    "id": 0,
                    "title": "Controlla orario classi",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/Orari/Provvisorio_2_Orario_classi_dal_12-09-19.pdf"
                },
                {
                    "id": 1,
                    "title": "Controlla orario docenti",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/Orari/Provvisorio_2_Orario_docenti_dal_12-09-19.pdf"
                },
                {
                    "id": 2,
                    "title": "Apri la circolare",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/Circ_orario_provvisorio_def.pdf"
                }
            ]
        },
        {
            "id": 1,
            "title": "Avvio anno scolastico 2019/2020",
            "thumbnail": "/static/img/campana.jpg",
            "description": "Si comunica che le lezioni avranno inizio giovedì 12 settembre secondo le seguenti modalità",
            "links": [
                {
                    "id": 0,
                    "title": "Apri la circolare",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/Avvio_A.S._2019-20.pdf"
                }
            ] 
        },
        {
            "id": 2,
            "title": "Piano delle attività 2019/2020",
            "thumbnail": "/static/img/calendario.jpg",
            "description": "Si comunica il piano delle attività per l’A.S. 2019-20 approvato nel collegio docenti del 5 settembre 2019.",
            "links": [
                {
                    "id": 0,
                    "title": "Apri la circolare",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/circ_piano_attivit%C3%A0_2019-20_DEF_2.pdf"
                }
            ] 
        },
        {
            "id": 3,
            "title": "Chiusura venerdì 6 settembre",
            "thumbnail": "/static/img/avvisonews.jpg",
            "description": "Venerdì 6 settembre l’Istituto resterà chiuso per l’intera giornata per lavori di manutenzione alla cabina elettrica.",
            "links": [
                {
                    "id": 0,
                    "title": "Apri la circolare",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/chiusura_6_settembre.pdf"
                }
            ]
        },
        {
            "id": 4,
            "title": "Incontro famiglie Progetto Erasmus+ KA2 “Il Mediterraneo: un mare di letture e immaginazione” A.S. 2019/20",
            "links": [
                {
                    "id": 0,
                    "title": "Incontro famiglie Progetto Erasmus+",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/Incontro_genitori_erasmus_2019-20_DEF.pdf"
                },
                {
                    "id": 1,
                    "title": "Il Mediterraneo: un mare di letture e immaginazione”",
                    "src": "http://majoranapa.edu.it/images/download/2019-20/CIRCOLARI/Incontro_genitori_erasmus_2019-20_DEF.pdf"
                }
            ]
        },
    ]
)

module.exports = {
    PostHistory: post 
}