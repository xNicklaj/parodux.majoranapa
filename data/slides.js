const slides = (
    [
        {
            "id": 0,
            "src": "/static/img/slide-0.png",
            "color": "#C6C6C6"
        },
        {
            "id": 1,
            "src": "/static/img/slide-1.png",
            "color": "#FAB900"
        },
        {
            "id": 2,
            "src": "/static/img/slide-2.png",
            "color": "#7DB231"
        },
        {
            "id": 3,
            "src": "/static/img/slide-3.png",
            "color": "#D7080F"
        }
    ]
)

module.exports = {
    Slides: slides 
}