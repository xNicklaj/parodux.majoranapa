import React, { useState } from 'react'

import '../static/style/foot.scss'

const Foot = props => {
    return (
        <footer>
            <div className='grid'>
                {props.children}
            </div>
        </footer>
    )
}

export default Foot