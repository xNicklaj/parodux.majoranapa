import React from 'react'

import '../static/style/card.scss'

const Card = props => {
    return (
        <div className={`card ${props.className ? props.className : ''}`} style={{background: props.background || 'green', color: props.color || 'white'}}>
            {props.children}
        </div>
    )
}

export default Card