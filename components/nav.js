import React, { useState } from 'react'

import '../static/style/nav.scss'

// Navbar. All children passed to this Object will be put into a list item
// and contained via a Flex Unordered List.
const Nav = props => {
  const [pagWidth, setPagWidth] = useState(0)
  const [pagOffset, setPagOffset] = useState(0)

  // List Item over effect
  const hover = e => {
    setPagWidth(e.target.offsetWidth)
    setPagOffset(e.target.offsetLeft)

  }

  // Reset effect
  const dehover = e => {
    setPagWidth(0)
  }
  
  let skey = 0;
  return ( 
    <nav>
      <ul>
          {
            props.children != null ?
              props.children.map(elem => {
                skey++;
                return <li onMouseOver={hover} onMouseOut={dehover} key={skey}>{elem}</li>
              }) :
              ''
          }
      </ul>
      <div className='pagination'>
          <div style={{width: pagWidth, left: pagOffset }}></div>
      </div>
    </nav>
  )
}

const Divider = <div className='divider'></div>

export default Nav
