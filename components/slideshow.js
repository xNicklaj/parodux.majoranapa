import React, {Component} from 'react'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'

import '../static/style/slideshow.scss'

class Showcase extends Component {
    INTERVAL_DELAY = 3000

    state = {
        index: 0,
        slideColor: '',
        slideSrc: ''
    }

    intervals = {
        slideshow: null
    }

    progressImg = () => {
        clearInterval(this.intervals.slideshow)
        let {index} = this.state
        let next = this.props.slides[(++index) % 4]

        const slideSrc = next.src
        const slideColor = next.color
        
        this.setState({index, slideSrc, slideColor})
        this.intervals.slideshow = setInterval(this.progressImg, this.INTERVAL_DELAY)
    }
    
    regressImg = () => {
        clearInterval(this.intervals.slideshow)
        let index = this.state.index == 0 ? this.props.slides.length : this.state.index
        let next = this.props.slides[(--index) % 4]
        const slideSrc = next.src
        const slideColor = next.color
        
        this.setState({index, slideSrc, slideColor})
        this.intervals.slideshow = setInterval(this.progressImg, this.INTERVAL_DELAY)
    }
    
    componentDidMount() {
        this.intervals.slideshow = setInterval(this.progressImg, this.INTERVAL_DELAY)
        
        this.setState({slideColor: this.props.basecolor, slideSrc: this.props.baseimg}) 
    }

    componentWillUnmount() {
        clearInterval(this.intervals.slideshow)
    }

    render() {
        return (
            <div className='slideshow' style={{fontSize: '10px'}}>
                <img src={this.state.slideSrc} alt='Immagine Slideshow' ></img>
                <div className='slideshow-control' style={{background: `${this.state.slideColor}`}}>
                    <div className='control' onClick={this.progressImg} ><FaChevronRight /></div>
                    <div className='control' onClick={this.regressImg} ><FaChevronLeft /></div>
                </div>
            </div>
        )
    }
}

export default Showcase