import React, { useState, useEffect } from 'react'
import { FaChevronDown, FaChevronRight } from 'react-icons/fa'

import '../static/style/collapsableNav.scss'

// Navbar. All children passed to this Object will be put into a list item
// and contained via a Flex Unordered List.
const CollapsableNav = props => {
  const [pagWidth, setPagWidth] = useState(0)
  const [pagOffset, setPagOffset] = useState(0)
  const [isExpanded, setExpanded] = useState(false)

  // List Item over effect
  const hover = e => {
    setPagWidth(e.target.offsetWidth)
    setPagOffset(e.target.offsetLeft)

  }

  // Reset effect
  const dehover = e => {
    setPagWidth(0)
  }

  const toggleExpanded = e => {
    if(![...document.querySelectorAll('li a')].includes(e.target))
      setExpanded(!isExpanded)
    else if([...document.querySelectorAll('li a')].includes(e.target) && isExpanded)
      collapse()
  }

  const outsideTouch = e => {
    if(!e.path.includes(document.querySelector('.collapsableNav')))
        collapse()
  }
  
  const collapse = () => {
      setExpanded(false)
  }

  useEffect(() => {
    document.addEventListener('mousedown', outsideTouch)
    return () => {
        document.removeEventListener('mousedown', outsideTouch)
    }
  })

  let skey = 0;
  return ( 
    <nav className='collapsableNav' style={{height: isExpanded ? `${3*props.children.length}em` : '3em'}} onClick={toggleExpanded} >
      <ul>
          {
            props.children != null ?
              props.children.map(elem => {
                skey++;
                if(props.children[props.cPage] === elem)
                    return (
                        <li onMouseOver={hover} onMouseOut={dehover} key={skey}>
                            {
                                isExpanded ? <FaChevronDown /> : <FaChevronRight />
                            }
                            {elem}
                        </li>
                    ) 
                return <li onMouseOver={hover} onMouseOut={dehover} key={skey}>{elem}</li>
              }) :
              ''
          }
      </ul>
      <div className='pagination'>
          <div style={{width: pagWidth, left: pagOffset }}></div>
      </div>
    </nav>
  )
}

const Divider = <div className='divider'></div>

export default CollapsableNav