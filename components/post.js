import React from 'react'

import '../static/style/post.scss'

const Post = props => {
    let post = props.src
    let lindex = 0
    return (
        <div key={lindex++} className={`postcard ${props.className ? props.className : ''}`}>
            {
                post.thumbnail ? (
                    <img src={post.thumbnail} className='post-thumb' alt='Post Media' />
                ) : ''
            }
            <h2>{post.title}</h2>
            {
                post.description ? (
                    <p>{post.description}</p>
                ) : ''
            }
            {
                post.links ? (
                    <ul>
                        {
                            post.links.map(link => <li key={link.id}><button onClick={() => window.open(link.src)}>{link.title}</button></li>)
                        }
                    </ul>
                ) : ''
            }
        </div>
    )
}


export default Post