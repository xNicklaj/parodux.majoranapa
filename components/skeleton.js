import React, {Component} from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { FaSearch } from 'react-icons/fa'
import Nav from '../components/nav'
import CollapsableNav from '../components/collapsablenav'
import Foot from '../components/foot'

import '../static/style/skeleton.scss'

class Skeleton extends Component {
    state = {
        scale: 1,
        bodyHeight: '',
        viewport: 'desktop',
    }

    
    handleResize = () => {
        const viewport = window.innerWidth >= 1201 ? 'desktop' : 'mobile'
        this.setState({viewport})
    }
    
    componentDidMount() {
        this.handleResize()
        window.addEventListener('resize', this.handleResize)
        
        window.addEventListener('scroll', this.debounce(this.handleScroll))
        const body = document.querySelector('.sticky-body')
        const bodyHeight = window.innerHeight - body.offsetTop
        this.setState({bodyHeight})
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.debounce(this.handleScroll))
    }
    
    debounce = (func, delay) => { 
        let debounceTimer 
        return function() { 
            const context = this
            const args = arguments 
            clearTimeout(debounceTimer) 
            debounceTimer 
            = setTimeout(() => func.apply(context, args), delay) 
        } 
    } 
    
    handleScroll = () => {
        const value = window.scrollY / document.querySelector('.thumbnail img').offsetHeight * .2 + 1
        this.setState({scale: value < 1.5 ? value : 1.5})
    }
    
    search = term => {
        console.log(term)
    }

    sendToUrl = (url) => {
        console.log(url)
        window.open(url, '_blank')
    }

    navs = [
        <Link href='/'><a >Home</a></Link>,
        <Link href='#'><a >Famiglia</a></Link>,
        <Link href='#'><a >Studenti</a></Link>,
        <Link href='#'><a >Personale Scuola</a></Link>,
        <Link href='#'><a >Progetti</a></Link>,
        <Link href='#'><a >Circolari</a></Link>,
        <Link href='#'><a >Biblioteca</a></Link>,
        <Link href='#'><a >PON</a></Link>,
        <Link href='#'><a >Chi siamo</a></Link>
    ]

    render()
    {
    return (
    <div >
        <Head>
            <title>Home</title>
            <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        </Head>
        <div className='thumbnail'>
            <img src='/static/img/majorana.jpg' alt='Majorana' style={{transform: `scale(${this.state.scale})`}}/>
        </div>
        <div className='sticky-body' style={{height: `${this.state.bodyHeight}`}}>
            <div className='headlights'>
                <div className='logo'>
                    <img src='/static/img/logoweb.png' alt='logo' />
                    <img src='/static/img/coccarde.png' style={{width: `5em`, height: `5em`}} alt='Logo 50 Anni' ></img>
                </div>
                <div className='search' >
                    <input className='search-input' type='text' placeholder='Cerca nel sito' onSubmit={e => {
                        e.preventDefault()
                        this.search(e.target.innerText)
                    }} />
                    <button className='search-button' title='Cerca'><FaSearch /></button>
                </div>
            </div>
            {
                this.state.viewport == 'desktop' ? 
                <Nav >
                    {
                        this.navs
                    }
                </Nav> :
                <CollapsableNav cPage={this.props.cPage}>
                    {
                        this.navs
                    }
                </CollapsableNav>
            }
            <div className='content'>
            {this.props.children}
            </div>
            <Foot>
            <div className='school-info'>
                <h1>Istituto Superiore Ettore Majorana</h1>
                <p>Via G. Astorino, 56, Palermo</p>
                <p>PEC: <a href='mailto:PAIS01600g@pec.istruzione.it'>PAIS01600g@pec.istruzione.it</a></p>
                <p>Tel: 091 516986</p>
                <p>Email: <a href='mailto:PAIS01600g@istruzione.it'>PAIS01600g@istruzione.it</a></p>
                <p>C.F.: 80015300827</p>
                <p>Cod. univoco fatturazione: UFAA5E</p>
                <p>Cod. Meccanografico: pais01600g</p>
                <p>Iban: IT03N0521604601000008014185</p>
            </div>
            <div className='programs'>
                <h1>I nostri corsi di studio</h1>
                <img src='/static/img/tecnico.png' alt='Logo Istituto Tecnico'/>
                <p>Agraria</p>
                <p>Chimica</p>
                <p>Informatica</p>
                <img src='/static/img/professionale.png' alt='Logo Istituto Professionale' />
                <p>Servizi per l'agricoltura</p>
                <p>Servizi Grafico-Pubblicitari</p>
                <p>Servizi commerciali</p>
                <img src='/static/img/liceo.png' alt='Logo Liceo' />
                <p>Scientifico Tradizionale</p>
                <p>Scientifico Scienze Applicate</p>
                <p>Scientifico Sportivo</p>
                <p>Artistico Grafico</p>
                <p>Artistico Audiovisivo Multimediale</p>
            </div>
            <div className='more'>
                <h1>Altri link</h1>
                <p><Link href='#'><a>Dirigenza</a></Link></p>
                <p><Link href='#'><a>Libri di Testo</a></Link></p>
                <p><a href='http://majoranapa.edu.it/images/download/menu_principale/Regolamento%20Istituto%202014.pdf'>Regolamento d'Istituto</a></p>
                <p><Link href='#'><a>URP</a></Link></p>
                <p><a href='http://majoranapa.edu.it/images/download/menu_principale/ATTO_DINDIRIZZO_DEL_DIRIGENTE_SCOLASTICO.pdf'>Atto d'Indirizzo</a></p>
                <p><a href='http://majoranapa.edu.it/images/download/menu_principale/Grafica_2017/Pieghevole_Majorana_2017.pdf'>Piani di Studi</a></p>
                <p><Link href='#'><a>L'istituto</a></Link></p>
                <p><Link href='#'><a>Come Raggiungerci</a></Link></p>
                <p><Link href='#'><a>Ruoli e Funzioni</a></Link></p>
                <p><Link href='#'><a>PTOF</a></Link></p>
                <p><a href='https://sites.google.com/a/majoranapa.gov.it/50-majorana-copia-sicurezza/'>Visita il libro digitale</a></p>
                <p><a href='http://sg20507.scuolanext.info'>Scuola Digitale</a></p>
            </div>
            <div className='subscriptions'>
                <h1>Iscrizioni Online</h1>
                <a><img src='/static/img/online.jpg' alt='Banner Iscrizioni Online' /></a>
                <h2 style={{color: `#D7080f`}}>ITI - PATF016014</h2>
                <p >Informatica</p>
                <p >Chimica</p>
                <h2 style={{color: `#0081C3`}}>ITA - PATA01601B</h2>
                <p>Agraria</p>
                <p>Agroalimentare</p>
                <h2 style={{color: `#7bb434`}} >IPA - PARA01601G</h2>
                <p>Servizi per l'Agricoltura</p>
                <p>Servizi Commerciali</p>
                <p>Operatore Grafico</p>
                <h2 style={{color: `#FBC114`}}>Liceo - PAPS016012</h2>
                <p>Scienze Applicate</p>
                <p>Sportivo</p>
                <p>Artistico</p>
            </div>
            <div className='administration'>
                <h1>Amministrazione Trasparente</h1>
                <p><a href='https://web.spaggiari.eu/sdg/app/default/trasparenza.php?sede_codice=paii0001&referer=http://www.majoranapa.gov.it'>Segreteria Digitale</a></p>
                <p><a href='https://web.spaggiari.eu/sdg/app/default/albo_pretorio.php?sede_codice=paii0001&referer=http://www.majoranapa.gov.it'>Albo Online</a></p>
            </div>
            <div className='privacy'>
                <h1>Privacy</h1>
                <img src='/static/img/GDPR.jpg' alt='Logo GDPR' />
                <p><a href='https://web.spaggiari.eu/sdg/app/default/view_documento.php?a=akVIEW_FROM_ID&id_documento=82961834&sede_codice=paii0001'>Registro trattamento Privacy</a></p>
                <p><a href='https://web.spaggiari.eu/sdg/app/default/view_documento.php?a=akVIEW_FROM_ID&id_documento=110583377&sede_codice=paii0001'>Nomina responsabile DPO</a></p>
                <p><a href='https://web.spaggiari.eu/sdg/app/default/view_documento.php?a=akVIEW_FROM_ID&id_documento=82962563&sede_codice=paii0001'>Nomina responsabile ARGO</a></p>
                <p><a href='https://web.spaggiari.eu/sdg/app/default/view_documento.php?a=akVIEW_FROM_ID&id_documento=83058748&sede_codice=paii0001'>Nomina responsabile Stat Consulting</a></p>
            </div>
            <div className='grads'>
                <h1>Graduatorie online</h1>
                <p><a href='#'>1a Fascia Docenti</a></p>
                <p><a href='#'>2a Fascia Docenti</a></p>
                <p><a href='#'>3a Fascia Docenti</a></p>
                <p><a href='#'>Sostegno</a></p>
                <p><a href='#'>1a Fascia ATA</a></p>
                <p><a href='#'>2a Fascia ATA</a></p>
                <p><a href='#'>3a Fascia ATA</a></p>
            </div>

            <style jsx>{`
                a:link {
                color: white;
                }
                
                a {
                color: white;
                }
            `}</style>
            </Foot>
        </div>
    </div>
    )}
}

export default Skeleton