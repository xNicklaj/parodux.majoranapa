const withSass = require('@zeit/next-sass')
module.exports = withSass({
  /* config options here */
  exportPathMap: function() {
    return {
      '/': { page: '/' }
    } 
  }
})
