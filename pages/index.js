import React from 'react'
import Head from 'next/head'

import Skeleton from '../components/skeleton'
import Showcase from '../components/slideshow'
import Card from '../components/card'
import Post from '../components/post'

import { Slides } from '../data/slides'
import { PostHistory } from '../data/postlist'

import '../static/style/index.scss'

// TO-DO: turn slideshow into own component and have it work through json objects.

class Home extends React.Component {  
  render() {
    return (<Skeleton cPage={0}>
      <Head>
        <title>Home | I.S. Ettore Majorana, Palermo </title>
        <meta name='description' content='Istituto Superiore Ettore Majorana, Palermo'/>
      </Head>
      <div className='mainview'>
        <Showcase baseimg='/static/img/slide-0.png' basecolor='#c6c6c6' slides={Slides} />
        <Card background='#5c6bc0' className='calamajo'>
          <img src='/static/img/Calamajo_bis.png' alt='Logo Scuola' />
          <button onClick={() => window.open()} title='Leggi il giornalino'>Leggi online il nostro giornalino scolastico</button>
          <button onClick={() => window.open('http://majoranapa.edu.it/images/download/2018-19/Circolari/calamajo_5_web.pdf')} title='Leggi ultimo volume' >Volume più recente</button>
        </Card>
      </div>

      <div className='readme'>
        <Card background='#D7080F' className='school'>
          <h2>Notizie in evidenza</h2>
          <button onClick={() => window.open('http://majoranapa.edu.it/images/download/2018-19/Esami_di_stato_2019_Pontillo_Serio_Spagnolo_Stimolo.pdf')} title='Esame di stato' >Nuovo esame di stato (2019)</button>
          <button >Piano annuale delle attività A.S. 2018-19</button>
          <button>Graduatoria alunni ammessi classi prime Liceo Sportivo A.S. 2019-20</button>
        </Card>
        <Card background='#65AE50' className='schedule'>
          <h2>Orario 2019-2020</h2>
          <button onClick={() => window.open('http://majoranapa.edu.it/images/download/2019-20/Orari/Provvisorio_2_Orario_classi_dal_12-09-19.pdf')}>Orario provvisorio classi dal 12 settembre</button>
          <button onClick={() => window.open('http://majoranapa.edu.it/images/download/2019-20/Orari/Provvisorio_2_Orario_docenti_dal_12-09-19.pdf')}>Orario provvisorio insegnanti dal 12 settembre</button>
        </Card>
        <Card background='#0080c3' className='erasmus'>
          <h2>Erasmus+</h2>
          <button onClick={() => window.open('http://majoranapa.edu.it/images/download/2018-19/Circolari/candidature_Erasmus_2019-20.pdf')}>Reclutamento alunni Erasmus+ A.S. 2019/20</button>
          <button onClick={() => window.open('http://majoranapa.edu.it/index.php/studenti-2/comenius/2017-2/agrinnovation')}>Agrinnovation KA1 VET</button>
          <button onClick={() => window.open('http://majoranapa.edu.it/index.php/studenti-2/comenius/2017/il-mediterraneo-mito-e-speranza')}>KA2 “Il Mediterraneo: mito e speranza”</button>
        </Card>
      </div>
      <div className='postlist'>
        {
          PostHistory.map(elem => <Post src={elem} />)
        }
      </div>
    </Skeleton>)
  }
}

export default Home
